﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TombstoneHelper.Example
{
    public static class RandomPersonFactory
    {
        private static Random _random = new Random();
        private static List<string> _firstNames = new List<string>() { "Steven", "Donnette", "Bertie", "Merlin", "Lanora", "Elissa", "Nicholle", "Anibal", "Sally", "Winona", "Lavinia", "Kim", "Starr", "Candelaria", "Kristofer", "Tabetha", "Catina", "Darryl", "Lu", "Nydia", "Dewayne", "Lakendra", "Jc", "Tommie", "Thanh", "Shawn", "Kaila", "Jackeline", "Piedad", "Nenita" };
        private static List<string> _lastNames = new List<string>() { "Mcdonald", "Vaughn", "Garcia", "Castillo", "Myers", "Sharp", "White", "Parker", "Hernandez", "Hunter", "Bowers", "Black", "Marsh", "Doyle", "Henry", "Lucas", "Ballard", "Mckenzie", "Martin", "Hodges" };

        public static Person CreateRandomPerson()
        {
            Person p = new Person();
            p.FirstName = _firstNames.ElementAt(_random.Next(_firstNames.Count));
            p.LastName = _lastNames.ElementAt(_random.Next(_lastNames.Count));
            p.Age = _random.Next(17, 40);
            return p;
        }

        public static List<Person> CreatRandomPersonsList(int n)
        {
            var result = new List<Person>();
            for (int i = 0; i < n; i++)
            {
                result.Add(CreateRandomPerson());
            }
            return result;
        }
    }
}
