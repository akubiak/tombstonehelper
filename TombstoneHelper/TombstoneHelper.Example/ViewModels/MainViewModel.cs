﻿using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TombstoneHelper.Example
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private List<Person> persons;
        public List<Person> Persons
        {
            get { return persons; }
            set { persons = value; OnPropertyChanged("Persons"); }
        }

        public void Initialize()
        {
            Persons = RandomPersonFactory.CreatRandomPersonsList(10);
        }

        public event PropertyChangedEventHandler PropertyChanged = default(PropertyChangedEventHandler);
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }

    public class MainViewModelDesign : MainViewModel
    {
        public MainViewModelDesign()
        {
            Persons = new List<Person>() { new Person() { FirstName = "Arek", LastName = "Kubiak", Age = 23 } };
        }
    }
}
