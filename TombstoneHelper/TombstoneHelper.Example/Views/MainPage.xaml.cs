﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TombstoneHelper.Example.Resources;
using System.Threading.Tasks;

namespace TombstoneHelper.Example
{
    public partial class MainPage : PhoneApplicationPage, ITombstoneHandler
    {
        public MainViewModel ViewModel { get; set; }

        public MainPage()
        {
            InitializeComponent();
            ViewModel = new MainViewModel();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                ViewModel.Initialize();
            }
            else
            {
                if (TombstoneManager.CheckIfPageRequiresRestore(this))
                {
                    await RestoreViewStateAsync();
                }
            }
            this.DataContext = ViewModel;
            base.OnNavigatedTo(e);
        }

        protected async override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode != NavigationMode.Back)
            {
                await SaveViewStateAsync();
            }
            base.OnNavigatedFrom(e);
        }

        public async Task SaveViewStateAsync()
        {
            State["MainPageViewModel"] = ViewModel;
        }

        public async Task RestoreViewStateAsync()
        {
            if (State.ContainsKey("MainPageViewModel"))
            {
                ViewModel = (MainViewModel)State["MainPageViewModel"];
            }
        }
    }
}