﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TombstoneHelper.ExampleStyleMVVM.Helpers
{
    public class StorageHelper
    {
        private StorageHelper()
        {
        }

        #region Singleton
        static private StorageHelper _instance = null;
        public static StorageHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new StorageHelper();
                }
                return _instance;
            }
        }
        #endregion
        public void SaveFile(string filename, object payload)
        {
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream rawStream = isf.CreateFile(filename))
                {
                    var data = JsonConvert.SerializeObject(payload);
                    StreamWriter writer = new StreamWriter(rawStream);
                    writer.Write(data);
                    writer.Flush();
                    writer.Close();
                }
            }
        }

        public T LoadFile<T>(string filename)
        {
            T result = default(T);
            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (isf.FileExists(filename))
                {
                    try
                    {
                        using (IsolatedStorageFileStream rawStream = isf.OpenFile(filename, FileMode.Open))
                        {
                            StreamReader reader = new StreamReader(rawStream);
                            var json = reader.ReadToEnd();
                            result = JsonConvert.DeserializeObject<T>(json);
                            reader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }

            return result;
        }
    }
}
