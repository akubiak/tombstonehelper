﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyleMVVM.DependencyInjection;
using StyleMVVM.ViewModel;
using TombstoneHelper.ExampleStyleMVVM.Helpers;
using System.Windows.Navigation;

namespace TombstoneHelper.ExampleStyleMVVM.ViewModels
{
    public class MainViewModel : PageViewModel, ITombstoneHandler
    {
        private List<Person> persons;
        public List<Person> Persons
        {
            get { return persons; }
            set { SetProperty(ref persons, value); }
        }

        protected override async void OnNavigatedTo(object sender, global::StyleMVVM.View.StyleNavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                Initialize();
            }
            else
            {
                if (TombstoneManager.CheckIfPageRequiresRestore(this))
                {
                    await RestoreViewStateAsync();
                }
            }
            base.OnNavigatedTo(sender, e);
        }

        protected override async void OnNavigatedFrom(object sender, global::StyleMVVM.View.StyleNavigationEventArgs e)
        {
            if (e.NavigationMode != NavigationMode.Back)
            {
                await SaveViewStateAsync();
            }
            base.OnNavigatedFrom(sender, e);
        }

        public void Initialize()
        {
            Persons = RandomPersonFactory.CreatRandomPersonsList(10);
        }

        public async Task SaveViewStateAsync()
        {
            StorageHelper.Instance.SaveFile("Persons", Persons);
        }

        public async Task RestoreViewStateAsync()
        {
            Persons = StorageHelper.Instance.LoadFile<List<Person>>("Persons");
        }
    }

    public class MainViewModelDesign : MainViewModel
    {
        public MainViewModelDesign()
        {
            Persons = new List<Person>() { new Person() { FirstName = "Arek", LastName = "Kubiak", Age = 23 } };
        }
    }
}
