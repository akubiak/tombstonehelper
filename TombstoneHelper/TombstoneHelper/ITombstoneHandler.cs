﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TombstoneHelper
{
    public interface ITombstoneHandler
    {
        Task SaveViewStateAsync();
        Task RestoreViewStateAsync();
    }
}
