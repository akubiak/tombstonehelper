﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace TombstoneHelper
{
    public class TombstoneManager
    {
        private static List<int> _pages = new List<int>();
        
        public static bool IsApplicationInstancePreserved { get; set; }

        ///<exception cref="ArgumentNullException"/>
        public static bool CheckIfPageRequiresRestore(object sender)
        {
            if(sender == null)
            {
                throw new ArgumentNullException("Sender can't be null");
            }
            if (!IsApplicationInstancePreserved && !_pages.Contains(sender.GetHashCode()))
            {
                _pages.Add(sender.GetHashCode());
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
